#!/usr/bin/env node

const cp = require('node:child_process');

const usage = () => { console.error(`Usage: npmon [-d delay-in-ms] command [command-args]`); process.exit(1); };

if (process.argv && process.argv.length < 3) usage();

const getDelay = (a, d = 60000) => { if (a[0] === '-d' && parseInt(a[1])) { a.shift(); return parseInt(a.splice(0, 1)); } else { return d; } };
const pad = m => m < 9 ? `0${m + 1}` : m + 1;
const args = process.argv.slice(2);
const checkDelay = getDelay(args);
const command = args.splice(0, 1)[0];
const getDateStr = () => {
    const d = new Date();
    return `${d.getFullYear()}-${pad(d.getMonth())}-${pad(d.getDate())} ${pad(d.getHours())}:${pad(d.getMinutes())}:${pad(d.getSeconds())}`;
}
const info = m => console.info(`[${getDateStr()}] ${m}`);
const warn = m => console.warn(`[${getDateStr()}] ${m}`);
const errr = m => console.err(`[${getDateStr()}] ${m}`);
const spawnProc = () => {
    let p = cp.fork(command, args);
    p.on('exit', spawnProc);
    return p;
};

if (typeof checkDelay !== 'number' || typeof command !== 'string' || command.length < 1) usage();

info(`Using delay (ms): ${checkDelay}`);
info(`Using JS module:  ${command} ${args.join(' ')}`);

let proc = spawnProc();

let t = setInterval(() => {
    info('Update check starting.');

    cp.exec('npm update', (err, o, e) => {
        if (err) errr(err);

        if (typeof o === 'string' && o !== null && o.length > 0) {
            const i = o.trim().indexOf('up to date');
            if (i === 0) {
                // We are up-to-date; do nothing.
            } else {
                // Something was updated; restart.
                proc.kill();
            }
        } else {
            warn(o, e);
        }

        info('Update check finished.');
    });
}, checkDelay);
