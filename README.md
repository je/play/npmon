# npmon

A simple NPM module that allows the user to execute a specified Node
module and monitor for NPM module (dependency) changes. When there is
a change, the Node process is restarted.

Monitoring is done by running `npm update`. If anything changes, the
running Node process will be killed and the specified Node module will
be executed (forked) again.

The default delay between NPM module checks is 60 seconds. This can be
changed by specifying a `-d` argument with a value in milliseconds:

    npmon -d 15000 mymodule.js myarg1 myarg2
